//
//  AirportManager.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

protocol AirportManagerProtocol: Observable {
    func retrieveAirports(latituted: String, longitude: String)
}

protocol AirportManagerObserverProtocol {
    func didRetrieveAirports(_ airports: Airports)
    func didFailToRetrieveAirports(error: Error)
}

class AirportManager: NSObject {
    static var shared = AirportManager()
    private let airportService: AirportService
    private let observers: Observer
    
    override init() {
        self.airportService = AirportService()
        self.observers = Observer()
    }
    
}

extension AirportManager: AirportManagerProtocol {
    func retrieveAirports(latituted: String, longitude: String) {
        airportService.getSearchResults(for: "\(latituted)", longitude: "\(longitude)") { [weak self] (airportList, error) in
            guard let strongSelf = self else { return }
            
            if let error = error {
                strongSelf.didFailToRetrieveAirports(error: error)
            } else {
                guard let airports = airportList else {
                    strongSelf.didFailToRetrieveAirports(error: NSError(domain: "com.arnold.airportListFailedToParse", code: -1, userInfo: nil))
                    return
                }
                strongSelf.didRetrieveAirports(airports)
            }
        }
    }
    
    func addObserver(_ object: AnyObject) {
        observers.addObserver(object)
    }
    
    func removeObserver(_ object: AnyObject) {
        observers.removeObserver(object)
    }
}

extension AirportManager: AirportManagerObserverProtocol {
    func didFailToRetrieveAirports(error: Error) {
        responder()?.didFailToRetrieveAirports(error: error)
    }
    
    func didRetrieveAirports(_ airports: Airports) {
        responder()?.didRetrieveAirports(airports)
    }
    
    private func responder() -> AirportManagerObserverProtocol? {
        for subscriber in observers.subscribers {
            if let subscriber = subscriber as? AirportManagerObserverProtocol { return subscriber }
        }
        return nil
    }
}
