//
//  LocationManager.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import CoreLocation

struct Location {
    let latitude: Double
    let longitude: Double
}

protocol LocationManagerProtocol: Observable {
    var currentLocation: Location { get }
    func retrieveLocation()
}

protocol LocationManagerObserverProtocol {
    func didRetrieveLocation(location: Location)
}

class LocationManager: NSObject, LocationManagerProtocol {
    private let sharedLocationManager: CLLocationManager
    private let observers: Observer
    
    //Default is Budapest
    var currentLocation = Location(latitude: 47.49801, longitude: 19.03991)
    
    override init() {
        sharedLocationManager = CLLocationManager()
        self.observers = Observer()
        
        super.init()
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied ||  CLLocationManager.authorizationStatus() == .notDetermined {
                sharedLocationManager.requestWhenInUseAuthorization()
            }
            sharedLocationManager.desiredAccuracy = kCLLocationAccuracyBest
            sharedLocationManager.delegate = self
            sharedLocationManager.startUpdatingLocation()
        } else {
            print("PLease turn on location services or GPS")
        }
    }
    
    func retrieveLocation() {
        sharedLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        sharedLocationManager.delegate = self
        sharedLocationManager.startUpdatingLocation()
    }
    
    func addObserver(_ object: AnyObject) {
        observers.addObserver(object)
    }
    
    func removeObserver(_ object: AnyObject) {
        observers.removeObserver(object)
    }
}

extension LocationManager {
    private func didRetrieveLocation(location: Location) {
        responder()?.didRetrieveLocation(location: location)
    }
    
    private func responder() -> LocationManagerObserverProtocol? {
        for subscriber in observers.subscribers {
            if let subscriber = subscriber as? LocationManagerObserverProtocol { return subscriber }
        }
        return nil
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        sharedLocationManager.stopUpdatingLocation()
        
        currentLocation = Location(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        didRetrieveLocation(location: currentLocation)
    }
}
