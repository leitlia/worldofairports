//
//  AirportService.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation
typealias AirportsListResult = (Airports?, Error?) -> ()

enum Endpoints: String {
    case base = "https://mikerhodes.cloudant.com/airportdb/"
    case search = "_design/view1/_search/"
}

class AirportService {
    let defaultSession: URLSession
    var dataTask: URLSessionDataTask?
    
    init() {
        defaultSession = URLSession(configuration: .default)
    }
    
    func getSearchResults(for lat: String, longitude: String, completion: @escaping AirportsListResult) {
        
        dataTask?.cancel()
        
        
        
        if var urlComponents = URLComponents(string: "\(Endpoints.base.rawValue)\(Endpoints.search.rawValue)geo") {
            urlComponents.query = "q=lon:[0 TO \(lat)] AND lat:[0 TO \(longitude)]"
            guard let url = urlComponents.url else { return }
            
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                if let error = error {
                    completion(nil, error)
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    do {
                        let airports = try JSONDecoder().decode(Airports.self, from: data)
                        completion(airports, nil)
                    } catch {
                        completion(nil, error)
                    }
                }
            }
            dataTask?.resume()
        }
    }
}
