//
//  Airport.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation
import CoreLocation

struct Airport: Codable {
    let lat: Double
    let lon: Double
    let name: String
    
    func distance(from latitude: Double, longitude: Double) -> Double {
        let coordinate1 = CLLocation(latitude:latitude, longitude: longitude)
        let coordinate2 = CLLocation(latitude: lat, longitude: lon)
        
        let distance = coordinate1.distance(from: coordinate2)
        return distance
    }
}
