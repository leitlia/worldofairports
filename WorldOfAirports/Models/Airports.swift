//
//  Airports.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

struct Airports: Codable {
    let totalRows: Int
    let bookmark: String
    let rows: [Row]
    
    init() {
        totalRows = 0
        bookmark = ""
        rows = [Row]()
    }
    
    private enum CodingKeys: String, CodingKey {
        case totalRows = "total_rows"
        case bookmark = "bookmark"
        case rows = "rows"
    }
    
    
}

struct Row: Codable {
    let id: String
    let order: [Double]
    let fields: Airport
}
