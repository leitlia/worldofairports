//
//  Observer.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

import Foundation

protocol Observable {
    func addObserver(_ object: AnyObject)
    func removeObserver(_ object: AnyObject)
}

class Observer: NSObject, Observable {
    private(set) var subscribers: [AnyObject]
    
    private var dispatchQueue: DispatchQueue
    
    override init() {
        subscribers = []
        dispatchQueue = DispatchQueue(label: "com.leitli.observer.access", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
    }
    
    func addObserver(_ object: AnyObject) {
        dispatchQueue.async(flags: .barrier) { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.subscribers.firstIndex(where: { (item) -> Bool in
                return item === object
            }) == nil {
                strongSelf.subscribers.append(object)
            }
        }
    }
    
    func removeObserver(_ object: AnyObject) {
        dispatchQueue.sync(flags: .barrier) { [weak self] in
            guard let strongSelf = self else { return }
            if let index = strongSelf.subscribers.firstIndex(where: { (item) -> Bool in
                return item === object
            }) {
                strongSelf.subscribers.remove(at: index)
            }
        }
    }
    
    //- (NSMethodSignature*)methodSignatureForSelector:(SEL)selector {
    //    NSMethodSignature* sig = [self.delegates.objectEnumerator.nextObject methodSignatureForSelector:selector];
    //    if (!sig) {
    //        // Create a dummy signiture in case no delegate is set yet
    //        // Reference: http://borkware.com/rants/agentm/elegant-delegation/
    //        sig=[NSMethodSignature signatureWithObjCTypes:"@^v^c"];
    //    }
    //    return sig;
    //}
    //
    //- (void)forwardInvocation:(NSInvocation*)invocation {
    //    dispatch_barrier_sync(self.delegateAccessQueue, ^{
    //        for (NSObject* delegate in self.delegates) {
    //            if ([delegate respondsToSelector:invocation.selector]) {
    //                [invocation invokeWithTarget:delegate];
    //            }
    //        }
    //    });
    //}
    
    
}
