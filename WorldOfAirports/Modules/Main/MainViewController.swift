//
//  MainViewController.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {
    var presenter: MainPresenterProtocol!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    
    @IBOutlet weak var listViewContainer: UIView!
    @IBOutlet weak var stackView: UIStackView!
    
    private var listViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let listViewController = listViewController else { return }
        addChild(listViewController)
        listViewContainer.addSubview(listViewController.view)
        addConstraints(for: listViewController.view, over: listViewContainer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.onViewDidLoad()
    }
    @IBAction func searchButtonPressed(_ sender: Any) {
        guard let longitudeText = longitudeTextField.text,
            let latitudeText = latitudeTextField.text else {
                return
        }
        presenter.onSearchButtonPressed(latitude: latitudeText, longitude: longitudeText)
    }
}

extension MainViewController: MainViewProtocol {
    
    func setup(with listViewController: UIViewController) {
        self.listViewController = listViewController
    }
    
    func updateView(with presentationModel: MainPresentationModelProtocol) {
        
    }
}
