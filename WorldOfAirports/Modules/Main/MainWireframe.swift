//
//  MainWireframe.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class MainWireframe {
    var view: (MainViewProtocol & UIViewController)!
    
    private var listModule: ListWireframeProtocol?
    
    init() {
        
        let interactor = MainInteractor(airportManager: AirportManager.shared)
        let presenter = MainPresenter()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.wireframe = self
        guard let main = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "Main") as? MainViewController else {
            return
        }
        
        view = main
        presenter.view = view
        view.presenter = presenter
        
        setupView()
    }
}

extension MainWireframe: MainWireframeProtocol {
    
    func setupView() {
        self.listModule = ListWireframe(airportManager: AirportManager.shared)
        view.setup(with: listModule!.listView())
    }
}

extension MainWireframe: MainModuleProtocol {
    func mainView() -> UIViewController {
        return view
    }
    
    
}
