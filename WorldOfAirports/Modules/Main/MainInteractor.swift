//
//  MainInteractor.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

class MainInteractor: NSObject {
    var presenter: MainPresenterProtocol?
    
    private let airportManager: AirportManagerProtocol
    
    init(airportManager: AirportManagerProtocol) {
        self.airportManager = airportManager
    }
}

extension MainInteractor: MainInteractorInputProtocol {
    func retrieveAirports(latitude: String, longitude: String) {
        airportManager.retrieveAirports(latituted: latitude, longitude: longitude)
    }
}
