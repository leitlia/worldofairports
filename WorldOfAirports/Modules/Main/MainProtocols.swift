//
//  MainProtocols.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

protocol MainInteractorInputProtocol: class {
    var presenter: MainPresenterProtocol? { get set }
    
    func retrieveAirports(latitude: String, longitude: String)
}

protocol MainInteractorOutputProtocol: class {
    
}

protocol MainPresenterProtocol: MainInteractorOutputProtocol {
    var view: MainViewProtocol? { get set }
    var wireframe: MainWireframeProtocol? { get set }
    var interactor: MainInteractorInputProtocol! { get set }
    
    func onViewDidLoad()
    func onSearchButtonPressed(latitude: String, longitude: String)
}

protocol MainPresentationModelProtocol: class {
    
}

protocol MainViewProtocol: class {
    var presenter: MainPresenterProtocol! { get set }
    
    func setup(with listViewController: UIViewController)
    func updateView(with presentationModel: MainPresentationModelProtocol)
}

protocol MainWireframeProtocol: MainModuleProtocol {
    var view: (MainViewProtocol & UIViewController)! { get set }
    
    func setupView()
}

protocol MainModuleProtocol: class {
    func mainView() -> UIViewController
}
