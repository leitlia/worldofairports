//
//  MainPresenter.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

class MainPresenter: NSObject {
    weak var view: MainViewProtocol?
    weak var wireframe: MainWireframeProtocol?
    var interactor: MainInteractorInputProtocol!
}

extension MainPresenter: MainPresenterProtocol {
    
    func onViewDidLoad() {
        wireframe?.setupView()
        interactor.retrieveAirports(latitude: "30", longitude: "50")
    }
    
    func onSearchButtonPressed(latitude: String, longitude: String) {
        interactor.retrieveAirports(latitude: latitude, longitude: longitude)
    }
    
}

extension MainPresenter: MainInteractorOutputProtocol {
    
}
