//
//  ListPresenter.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class ListPresenter: NSObject {
    weak var view: ListViewProtocol?
    weak var wireframe: ListWireframeProtocol?
    var interactor: ListInteractorInputProtocol!
    
    var presentationModel = ListPresentationModel()
    
}

extension ListPresenter: ListPresenterProtocol {
    func onViewDidLoad() {
        updateView(with: presentationModel)
    }
    
    func onViewWillAppear() {
        
    }
    
    func onSelected(item: Airport) {
        wireframe?.openDetails(with: item)
    }
}

extension ListPresenter: ListInteractorOutputProtocol {
    func didRetrieveLocation(location: Location) {
        let builder = ListPresentationModelBuilder(model: presentationModel)
        builder.currentLocation = location
        let sortedRows = builder.rows.sorted { (row1, row2) -> Bool in
            let location = self.presentationModel.currentLocation
            let distance1 = row1.fields.distance(from: location.latitude, longitude: location.longitude)
            let distance2 = row2.fields.distance(from: location.latitude, longitude: location.longitude)
            return distance1 > distance2
        }
        builder.rows = sortedRows
        updateView(with: builder.build())
    }
    
    func didRetrieveAirports(rows: [Row]) {
        let builder = ListPresentationModelBuilder(model: presentationModel)
        let sortedRows = rows.sorted { (row1, row2) -> Bool in
            let location = self.presentationModel.currentLocation
            let distance1 = row1.fields.distance(from: location.latitude, longitude: location.longitude)
            let distance2 = row2.fields.distance(from: location.latitude, longitude: location.longitude)
            return distance1 > distance2
        }
        builder.rows = sortedRows
        updateView(with: builder.build())
    }
}

extension ListPresenter {
    private func updateView(with presentationModel: ListPresentationModel) {
        DispatchQueue.main.async {
            self.presentationModel = presentationModel
            self.view?.updateView(with: presentationModel)
        }
    }
}
