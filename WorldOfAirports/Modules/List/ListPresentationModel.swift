//
//  ListPresentationModel.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

class ListPresentationModel: ListPresentationModelProtocol {
    let rows: [Row]
    let currentLocation: Location
    init() {
        self.rows = [Row]()
        self.currentLocation = Location(latitude: 0, longitude: 0)
    }
    
    init(rows: [Row],
         currentLocation: Location) {
        self.rows = rows
        self.currentLocation = currentLocation
    }
    
    init(builder: ListPresentationModelBuilder) {
        self.rows = builder.rows
        self.currentLocation = builder.currentLocation
    }
}

class ListPresentationModelBuilder: ListPresentationModelProtocol {
    var rows: [Row]
    var currentLocation: Location
    
    init(model: ListPresentationModel) {
        self.rows = model.rows
        self.currentLocation = model.currentLocation
    }
    
    func build() -> ListPresentationModel {
        return ListPresentationModel(builder: self)
    }
}
