//
//  ListProtocols.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

protocol ListInteractorInputProtocol: class {
    var presenter: ListPresenterProtocol? { get set }
}

protocol ListInteractorOutputProtocol: class {
    func didRetrieveAirports(rows: [Row])
    func didRetrieveLocation(location: Location)
}

protocol ListPresenterProtocol: ListInteractorOutputProtocol {
    var view: ListViewProtocol? { get set }
    var wireframe: ListWireframeProtocol? { get set }
    var interactor: ListInteractorInputProtocol! { get set }
    
    func onViewDidLoad()
    func onSelected(item: Airport)
    func onViewWillAppear()
}

protocol ListPresentationModelProtocol: class {
    var rows: [Row] { get }
    var currentLocation: Location { get }
}

protocol ListViewProtocol: class {
    var presenter: ListPresenterProtocol! { get set }
    
    func updateView(with presentationModel: ListPresentationModelProtocol)
}

protocol ListWireframeProtocol: ListModuleProtocol {
    var view: ListViewController! { get set }
    
    func openDetails(with class: Airport)
}

protocol ListModuleProtocol: class {
    func listView() -> UIViewController
}
