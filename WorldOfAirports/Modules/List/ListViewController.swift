//
//  ListViewController.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class ListViewController: BaseViewController {
    var presenter: ListPresenterProtocol!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var presentationModel: ListPresentationModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.onViewWillAppear()
    }
}

extension ListViewController: ListViewProtocol {
    
    func updateView(with presentationModel: ListPresentationModelProtocol) {
        self.presentationModel = presentationModel as? ListPresentationModel
        
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let items = presentationModel?.rows
        guard let item = items?[indexPath.row] else { return }
    }
}

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presentationModel?.rows.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListViewCellIdentifier", for: indexPath)
        
        let items = presentationModel?.rows
        
        guard let item = items?[indexPath.row] else { return cell }
        
        cell.textLabel?.text = item.fields.name
        cell.detailTextLabel?.text = "distance: \(item.fields.lat), \(item.fields.lon)"
        
        return cell
    }
}
