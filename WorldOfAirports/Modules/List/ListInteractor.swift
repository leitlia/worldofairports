//
//  ListInteractor.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import Foundation

class ListInteractor {
    var presenter: ListPresenterProtocol?
    
    private let airportManager: AirportManagerProtocol
    private let locationManager: LocationManagerProtocol
    
    init(airportManager: AirportManagerProtocol,
         locationManager: LocationManagerProtocol) {
        self.airportManager = airportManager
        self.locationManager = locationManager
        
        airportManager.addObserver(self)
        locationManager.addObserver(self)
    }
    
    deinit {
        airportManager.removeObserver(self)
        locationManager.removeObserver(self)
    }
}

extension ListInteractor: ListInteractorInputProtocol {
}

extension ListInteractor: AirportManagerObserverProtocol {
    func didRetrieveAirports(_ airports: Airports) {
        presenter?.didRetrieveAirports(rows: airports.rows)
    }
    
    func didFailToRetrieveAirports(error: Error) {
        
    }
}

extension ListInteractor: LocationManagerObserverProtocol {
    func didRetrieveLocation(location: Location) {
        presenter?.didRetrieveLocation(location: location)
    }
}
