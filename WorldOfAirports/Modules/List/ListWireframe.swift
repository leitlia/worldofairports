//
//  ListWireframe.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class ListWireframe: NSObject {
    var view: ListViewController!
    var navigationController: UINavigationController!
    
    init(airportManager: AirportManager) {
        
        guard let listView = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "ListView") as? ListViewController else {
            super.init()
            return
        }
        
        let locationManager = LocationManager()
        
        let interactor = ListInteractor(airportManager: airportManager,
                                        locationManager: locationManager)
        let presenter = ListPresenter()
        
        super.init()
        
        view = listView
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.wireframe = self
        view.presenter = presenter
    }
}

extension ListWireframe: ListWireframeProtocol {
    func openDetails(with class: Airport) {
    }
}

extension ListWireframe: ListModuleProtocol {
    func listView() -> UIViewController {
        return view
    }
    
    
}
