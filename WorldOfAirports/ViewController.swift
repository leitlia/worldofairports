//
//  ViewController.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mainNavigation = MainWireframe()
        add(child: mainNavigation.mainView(), to: view)
    }


}

