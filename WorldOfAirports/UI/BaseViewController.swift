//
//  BaseViewController.swift
//  WorldOfAirports
//
//  Created by Arnold Leitli on 2019. 07. 24..
//  Copyright © 2019. Arnold Leitli. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    func add(child viewController:UIViewController, to containerView: UIView) {
        add(child: viewController, to: containerView, forViewToCover: containerView)
    }
    
    func add(child viewController:UIViewController, to containerView: UIView, forViewToCover coveredView:UIView) {
        guard let childView = viewController.view else { return }
        viewController.willMove(toParent: self)
        childView.frame = coveredView.frame
        containerView.addSubview(childView)
        addChild(viewController)
        viewController.didMove(toParent: self)
        
        addConstraints(for: childView, over: coveredView)
    }
    
    func addConstraints(for view: UIView, over coveredView: UIView) {
        view.topAnchor.constraint(equalTo: coveredView.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: coveredView.bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: coveredView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: coveredView.rightAnchor).isActive = true
    }
}
