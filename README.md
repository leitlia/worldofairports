
# WorldOfAirports

This porject is pretty overshoots, what this application needed to be as a simple listing app. On the architecture level try to represent the rules that can be followed for sustainable, maintainable, and scalable product. The different modules can be used different part of the future states of the app as they can be reused anywhere else, in any size, or in different need.

# Installation

* clone the repo
* build/run

# Architecture

 * I used a little bit of a modified VIPER architecture, where basically this is the root of my thoughts of flow:
https://www.objc.io/issues/13-architecture/viper/

 * tried to present my knowledge using different patterns:

	 * Protocol Oriented programming.
	 * Dependency Injection, for inversion of dependencies
	 * Graph build up of the modules of the code
	 * Singleton pattern
	 * Observer pattern
These are very good for reusability, encapsulation, and definitley for Testing
